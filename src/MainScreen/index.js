import React from 'react';
import { Button, View, Text } from 'react-native';

export default class MainScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Main Screen</Text>
        <Button
          title="Go to Home"
          onPress={() => this.props.navigation.navigate('Main')}
        />
      </View>
    );
  }
}