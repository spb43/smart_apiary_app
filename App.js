import React, { Component, Fragment} from 'react';
import { StyleSheet, Button, Text, View } from 'react-native';
import { createBottomTabNavigator } from 'react-navigation';
import { Footer, FooterTab, Icon, StyleProvider } from 'native-base';
import getTheme from './native-base-theme/components';
import custom from './native-base-theme/variables/custom';

import MainScreen from './src/MainScreen'
import HiveScreen from './src/HiveScreen'
import ApiaryScreen from './src/ApiaryScreen'
import SettingsScreen from './src/SettingsScreen'
import Header from './src/Header'


const routes = {
  Main: {
    screen: MainScreen,
    icon: "flower"
  }/*
  Apiary: {
    screen: ApiaryScreen,
    icon: "ios-add-circle-outline"
  },
  Hive: {
    screen: HiveScreen,
    icon: "calendar"
  },
  Settings: {
    screen: SettingsScreen,
    icon: "ios-settings"
  }*/
}

const RootStack = createBottomTabNavigator(routes, {
  tabBarComponent: (props) => (
    <TabBar {...props} routes={routes} />
  )
});

class TabBar extends React.Component {

  render() {
    const current = this.props.navigation.state.index;
    const { routes } = this.props;
    return (
      <Footer>
        <FooterTab
          tabBarTextColor={"#000"}
          tabBarTextSize={10}
        >
          {
            Object.keys(routes).map((route, index) => ( 
              <Button 
                key={route}
                onPress={() => this.props.navigation.navigate(route) }
                active={index === current}
              >
                <Icon name={routes[route].icon} />
                <Text>{route}</Text>
              </Button>
            ))
          }
        </FooterTab>
      </Footer>
    )
  }
}

export default class App extends React.Component {

  constructor() {
    super();
    this.state = {
      isReady: false
    };
  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("native-base/Fonts/Ionicons.ttf"),
      Fredoka: require("./asserts/fonts/Fredoka.ttf")
    });
    this.setState({ isReady: true });
  }

  render() {
    if (!this.state.isReady) {
      return <Expo.AppLoading />;
    }
    return (
      <StyleProvider style={getTheme(custom)}>
        <Fragment>
          <Header />
          <RootStack />
        </Fragment>
       </StyleProvider>
    );
  }

  /*
  /*showTextHandler = () => {
    <Text>Smart_Apiary_APP</Text>
  }

  render() {
    return (

      <Text>Smart_Apiary_APP</Text>


      );
      
      
      /*
      <View style={styles.container}>
        <Text>Smart_Apiary_APP</Text>
      <Button 
            containerStyle={{padding:10, height:45, overflow:'hidden', borderRadius:4, backgroundColor: 'white'}}
            title = "Show text" onPress={this.showTextHandler}/>
      </View>
    );
  }*/
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});



